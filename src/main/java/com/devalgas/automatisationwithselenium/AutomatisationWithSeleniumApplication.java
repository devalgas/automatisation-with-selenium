package com.devalgas.automatisationwithselenium;

import com.devalgas.automatisationwithselenium.utils.EventReporter;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

//@SpringBootApplication
public class AutomatisationWithSeleniumApplication {

    public static void main(String[] args) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/webdriver/chromedriver.exe");
        EventFiringWebDriver driver = new EventFiringWebDriver(new ChromeDriver(options));
        driver.register(new EventReporter());
        driver.get("https://the-internet.herokuapp.com/");
        driver.quit();
//        SpringApplication.run(AutomatisationWithSeleniumApplication.class, args);
    }

}
